package com.example.chris.interactors.network;

import com.example.chris.entities.WeatherData;

import io.reactivex.Single;

public interface WeatherNetworkInteractor {

    /**
     * Metodo para obtener los datos
     *
     * @param city
     * @return
     */
    Single<WeatherData> getWeatherData(String city);
}
