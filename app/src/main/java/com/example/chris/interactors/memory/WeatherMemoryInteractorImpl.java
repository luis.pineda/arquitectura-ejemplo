package com.example.chris.interactors.memory;

import com.example.chris.entities.WeatherData;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class WeatherMemoryInteractorImpl
        implements WeatherMemoryInteractor {

    //region Variables

    BehaviorSubject<WeatherData> observable;

    WeatherData weatherData;

    //endregion

    //region Constructor de la clase

    /**
     * Inyeccion del constructor de la clase
     */
    @Inject
    public WeatherMemoryInteractorImpl() {

        observable = BehaviorSubject.create();

    }

    //endregion

    //region Sobrecarga de los metodos de la interface

    /**
     * Metodo para almacenar los datos
     *
     * @param weatherData
     */
    @Override
    public void saveData(WeatherData weatherData) {

        this.weatherData = weatherData;

        observable.onNext(weatherData);

    }

    /**
     * Metodo para obtener los datos
     *
     * @return
     */
    @Override
    public Maybe<WeatherData> getWeatherData() {
        return weatherData == null ? Maybe.empty() : Maybe.just(weatherData);
    }

    /**
     * Metodo para obtener los datos del observable
     *
     * @return
     */
    @Override
    public Observable<WeatherData> getWeatherDataObservable() {
        return observable;
    }

    //endregion

}