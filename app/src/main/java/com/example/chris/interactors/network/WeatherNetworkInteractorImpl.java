package com.example.chris.interactors.network;

import com.example.chris.entities.WeatherData;
import com.example.chris.interactors.database.WeatherDatabaseInteractor;
import com.example.chris.interactors.memory.WeatherMemoryInteractor;
import com.example.chris.services.networking.ApiService;
import com.example.chris.services.session.SessionService;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Interactuador para el consumo del WebService
 */
public class WeatherNetworkInteractorImpl
        implements WeatherNetworkInteractor {

    //region Variables

    ApiService apiService;

    SessionService sessionService;

    WeatherDatabaseInteractor weatherDatabaseInteractor;

    WeatherMemoryInteractor weatherMemoryInteractor;

    //endregion

    //region Constructor de la clase

    /**
     * Inyeccion del constructor de la clase
     */
    @Inject
    public WeatherNetworkInteractorImpl(ApiService apiService,
                                        SessionService sessionService,
                                        WeatherMemoryInteractor weatherMemoryInteractor,
                                        WeatherDatabaseInteractor weatherDatabaseInteractor) {

        this.apiService = apiService;

        this.sessionService = sessionService;

        this.weatherMemoryInteractor = weatherMemoryInteractor;

        this.weatherDatabaseInteractor = weatherDatabaseInteractor;

    }

    //endregion

    //region Sobrecarga de los metodos de la interface

    /**
     * Metodo para obtener los datos
     *
     * @param city
     * @return
     */
    @Override
    public Single<WeatherData> getWeatherData(String city) {
        return apiService.getWeather(city)
                .map(WeatherData::copyFromResponse)
                .doOnSuccess(data -> sessionService.saveLocation(data.name))
                .doOnSuccess(weatherDatabaseInteractor::saveData)
                .doOnSuccess(weatherMemoryInteractor::saveData);
    }
    //endregion

}