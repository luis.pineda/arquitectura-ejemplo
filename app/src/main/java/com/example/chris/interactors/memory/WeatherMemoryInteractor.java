package com.example.chris.interactors.memory;

import com.example.chris.entities.WeatherData;

import io.reactivex.Maybe;
import io.reactivex.Observable;

public interface WeatherMemoryInteractor {

    /**
     * Metodo para obtener los datos
     *
     * @return
     */
    Maybe<WeatherData> getWeatherData();

    /**
     * Metodo para obtener los datos del observable
     *
     * @return
     */
    Observable<WeatherData> getWeatherDataObservable();

    /**
     * Metodo para almacenar los datos
     *
     * @param weatherData
     */
    void saveData(WeatherData weatherData);
}
