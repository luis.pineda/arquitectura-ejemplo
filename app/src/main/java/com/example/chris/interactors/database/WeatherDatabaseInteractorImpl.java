package com.example.chris.interactors.database;

import com.example.chris.entities.WeatherData;
import com.example.chris.interactors.memory.WeatherMemoryInteractor;
import com.example.chris.services.persistence.WeatherDatabase;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.schedulers.Schedulers;

public class WeatherDatabaseInteractorImpl
        implements WeatherDatabaseInteractor {

    //region Variables

    WeatherDatabase weatherDatabase;

    WeatherMemoryInteractor weatherMemoryInteractor;

    //endregion

    //region Constructor de la clase

    /**
     * Inyeccion del constructor de la clase
     */
    @Inject
    public WeatherDatabaseInteractorImpl(WeatherDatabase weatherDatabase,
                                         WeatherMemoryInteractor weatherMemoryInteractor) {

        this.weatherDatabase = weatherDatabase;

        this.weatherMemoryInteractor = weatherMemoryInteractor;

    }

    //endregion

    //region Sobrecarga de los metodos de la interface

    /**
     * Metodo para obtener los datos
     *
     * @param name
     * @return
     */
    @Override
    public Maybe<WeatherData> getWeatherData(String name) {
        return weatherDatabase.weatherDao()
                .getWeather(name)
                .subscribeOn(Schedulers.io())
                .doOnSuccess(weatherMemoryInteractor::saveData);
    }

    /**
     * Metodo para almacenar los datos
     *
     * @param weatherData
     */
    @Override
    public void saveData(WeatherData weatherData) {
        weatherDatabase.weatherDao()
                .saveData(weatherData);
    }
    //endregion

}