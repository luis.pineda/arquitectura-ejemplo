package com.example.chris.sections.clima.fragments.wind;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.chris.ExampleApp;
import com.example.chris.R;
import com.example.chris.dagger.modules.sections.FragmentWindModule;
import com.example.chris.entities.WeatherData;
import com.example.chris.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentWind extends BaseFragment<FragmentWindPresenter> implements FragmentWindView {

    @BindView(R.id.tv_wind_speed)
    TextView tvWindSpeed;

    @BindView(R.id.tv_wind_direction)
    TextView tvWindDirection;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();
        presenter.onBind();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viento, container, false);
        ButterKnife.bind(this, view);
        presenter.onBind();
        return view;
    }

    private void injectDependencies() {
        ExampleApp.getAppComponent().plus(new FragmentWindModule(this)).inject(this);
    }

    @Override
    public void showWind(WeatherData forecastEntity) {
        tvWindSpeed.setText(String.format(getString(R.string.wind_speed_format), forecastEntity.wind.speed));
        tvWindDirection.setText(String.format(getString(R.string.wind_direction_format), forecastEntity.wind.deg));
    }
}
