package com.example.chris.sections.clima;

import android.os.Bundle;

import androidx.fragment.app.FragmentManager;

import com.example.chris.R;
import com.example.chris.base.GeneralBaseActivity;
import com.example.chris.base.InterfaceActivity;
import com.example.chris.base.InterfaceView;
import com.example.chris.global.TagsFragments;
import com.example.chris.sections.clima.fragments.FragmentMainWeather;

public class ClimaActivity
        extends GeneralBaseActivity
        implements InterfaceView,
        InterfaceActivity {

    /**
     * Metodo sobre cargado del sistema
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Colocando el contenido de la actividad
        setContentView(R.layout.activity_container_general);

        super.onCreate(savedInstanceState);

    }

    /*
    #############################################################################################
    Implementacion de la interface InterfaceActivity
    Metodos de manejo de contenido
    #############################################################################################
    */

    /**
     * Inicializa la interfaz de usuario
     */
    @Override
    public void setupUI() {

        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentMainWeather fragmentMainWeather = new FragmentMainWeather();

        fragmentManager
                .beginTransaction()
                .replace(R.id.rlyGeneralContainer,
                        fragmentMainWeather,
                        TagsFragments.tagFragMainWeather)
                .commitAllowingStateLoss();

    }


}
