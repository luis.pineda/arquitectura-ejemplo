package com.example.chris.sections.clima.fragments.location;

import com.example.chris.base.InterfacePresenter;

public interface FragmentLocationPresenter extends InterfacePresenter {

    void onBind();

    void changeLocation(String name);

}
