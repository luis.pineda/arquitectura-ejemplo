package com.example.chris.sections.clima.fragments.weather;

import com.example.chris.base.InterfaceView;
import com.example.chris.entities.WeatherData;

public interface FragmentWeatherView extends InterfaceView {

    void showWeather(WeatherData weatherData);

}
