package com.example.chris.sections.clima.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.chris.R;
import com.example.chris.sections.adapters.SectionsPagerAdapter;
import com.example.chris.sections.clima.fragments.location.FragmentLocation;
import com.example.chris.sections.clima.fragments.weather.FragmentWeather;
import com.example.chris.sections.clima.fragments.wind.FragmentWind;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentMainWeather
        extends Fragment {

    /**
     * #############################################################################################
     * Definición de variables y controles
     * #############################################################################################
     */

    @BindView(R.id.vwpContainerTabs)
    ViewPager vwpContainerTabs;

    @BindView(R.id.tabContentTabs)
    TabLayout tabContentTabs;

    private AppBarLayout ablGeneralContainer;

    /**
     * #############################################################################################
     * Constructor  de  la clase
     * #############################################################################################
     */
    public FragmentMainWeather() {

    }

    /*
    #############################################################################################
    Metodo sobrecargados del sistema
    #############################################################################################
    */

    /**
     * Metodo sobre cargado del sistema que es llamado cuando se crea la vista
     *
     * @param savedInstanceState
     */

    /**
     * @param inflater
     * @param container
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.ui_viewpager_tabs, container, false);

        ButterKnife.bind(this, view);

        ablGeneralContainer = ((View) Objects.requireNonNull(container).getParent()).findViewById(R.id.ablGeneral);

        setupViewPager(vwpContainerTabs);

        tabContentTabs.setupWithViewPager(vwpContainerTabs);

        setupTabIcons();

        return view;

    }

    /*
    #############################################################################################
    Metodo sobrecargados del sistema
    #############################################################################################
    */

    /**
     * Metodo sobre cargado del sistema que es llamado cuando se destruye la vista
     */
    @Override
    public void onDestroyView() {

        if (ablGeneralContainer != null)

            ablGeneralContainer.removeView(tabContentTabs);

        super.onDestroyView();

    }

    /*
    #############################################################################################
    Metodo propios de la clase
    #############################################################################################
    */

    /**
     * Metodo que inicializa las visor de paginas del fragmento
     *
     * @param viewPager
     */
    private void setupViewPager(@NonNull ViewPager viewPager) {

        viewPager.setOffscreenPageLimit(3);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        //FragmentWeather

        FragmentWeather fragmentWeather = new FragmentWeather();

        sectionsPagerAdapter.addFragment(fragmentWeather, "Clima");

        //FragmentWind

        FragmentWind fragmentWind = new FragmentWind();

        sectionsPagerAdapter.addFragment(fragmentWind, "Viento");

        //FragmentLocation

        FragmentLocation fragmentLocation = new FragmentLocation();

        sectionsPagerAdapter.addFragment(fragmentLocation, "Localizacion");

        //Agregado del Adaptador

        viewPager.setAdapter(sectionsPagerAdapter);

    }


    private void setupTabIcons() {

        TextView tabClima = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.ui_tab, null);
        tabClima.setText("Clima");
        tabClima.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_moon, 0, 0);
        tabContentTabs.getTabAt(0).setCustomView(tabClima);

        TextView tabViento = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.ui_tab, null);
        tabViento.setText("Viento");
        tabViento.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_wind, 0, 0);
        tabContentTabs.getTabAt(1).setCustomView(tabViento);

        TextView tabLocalizacion = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.ui_tab, null);
        tabLocalizacion.setText("Localizacion");
        tabLocalizacion.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_location, 0, 0);
        tabContentTabs.getTabAt(2).setCustomView(tabLocalizacion);

    }

}
