package com.example.chris.sections.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    /**
     * #############################################################################################
     * Definición de variables y controles
     * #############################################################################################
     */
    private final List<Fragment> fragments = new ArrayList<>();

    private final List<String> titulosFragmentos = new ArrayList<>();

    /**
     * Constructor de la sub clase
     *
     * @param fragmentManager
     */
    public SectionsPagerAdapter(FragmentManager fragmentManager) {

        super(fragmentManager);

    }

    /**
     * Metodo que obtiene la posicion del item
     *
     * @param position
     * @return
     */
    @Override
    public Fragment getItem(int position) {

        return fragments.get(position);

    }

    /**
     * Metodo que obtiene el conteo de items
     *
     * @return
     */
    @Override
    public int getCount() {

        return fragments.size();

    }

    /**
     * Metodo que agrega un fragmento
     *
     * @param fragment
     * @param title
     */
    public void addFragment(Fragment fragment, String title) {

        fragments.add(fragment);

        titulosFragmentos.add(title);

    }

    /**
     * Metodo que obtiene el titulo de la posicion
     *
     * @param position
     * @return
     */
    @Override
    public CharSequence getPageTitle(int position) {

        return titulosFragmentos.get(position);

    }
}