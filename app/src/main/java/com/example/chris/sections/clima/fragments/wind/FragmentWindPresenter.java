package com.example.chris.sections.clima.fragments.wind;

import com.example.chris.base.InterfacePresenter;

public interface FragmentWindPresenter extends InterfacePresenter {

    void onBind();

}
