package com.example.chris.sections.clima.fragments.location;

import com.example.chris.base.InterfaceView;
import com.example.chris.entities.WeatherData;

public interface FragmentLocationView extends InterfaceView {

    void showLocation(WeatherData forecastEntity);

}
