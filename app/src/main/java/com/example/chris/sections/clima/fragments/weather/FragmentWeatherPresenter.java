package com.example.chris.sections.clima.fragments.weather;

import com.example.chris.base.InterfacePresenter;

public interface FragmentWeatherPresenter extends InterfacePresenter {

    void onBind();

}
