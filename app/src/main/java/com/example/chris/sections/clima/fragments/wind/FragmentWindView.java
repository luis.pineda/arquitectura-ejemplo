package com.example.chris.sections.clima.fragments.wind;

import com.example.chris.base.InterfaceView;
import com.example.chris.entities.WeatherData;

public interface FragmentWindView extends InterfaceView {

    void showWind(WeatherData forecastEntity);

}
