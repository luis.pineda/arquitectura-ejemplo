package com.example.chris.sections.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.core.view.GravityCompat;
import androidx.fragment.app.FragmentManager;

import com.example.chris.R;
import com.example.chris.base.HomeBaseActivity;
import com.example.chris.base.InterfaceActivity;
import com.example.chris.base.InterfaceView;
import com.example.chris.global.TagsFragments;
import com.example.chris.sections.clima.ClimaActivity;
import com.example.chris.sections.home.fragments.FragmentHome;

import java.util.Objects;

public class HomeActivity
        extends HomeBaseActivity
        implements InterfaceView,
        InterfaceActivity {

    /**
     * Metodo sobre cargado del sistema
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Colocando el contenido de la actividad
        setContentView(R.layout.activity_container_home);

        super.onCreate(savedInstanceState);

        //Llamada al metodo que inicializa el menu de navegacion
        setupNavigation();

    }

    /*
    #############################################################################################
    Metodos de inicializacion de contenidos
    #############################################################################################
    */

    /**
     * Sobrecarga del metodo que inicializa el menu de navegacion desplegable
     */
    public void setupNavigation() {

        //Agregado el menu de la app
        Objects.requireNonNull(navigationViewHome).inflateMenu(R.menu.menu_nav);

        //Agregado el encabezado
        Objects.requireNonNull(navigationViewHome).inflateHeaderView(R.layout.ui_nav_header);

        setupDrawerContent();

    }

    /**
     * Sobrecarga de metodo del sistema que se intercepta
     * para crear las Opciones del menu en el Toolbar
     *
     * @param menu Recibe un parametro tipo Menu
     *             Para inflar el ToolBar
     * @return Regresa un Boolean al realizar la acción
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_toolbar, menu);

        return true;

    }

    /**
     * Sobrecarga de metodo del sistema que se intercepta
     * para abrir el menu de navegacion desplegable
     *
     * @param menuItem Recibe un parametro de tipo MenuItem
     *                 Para abrir el Drawer
     * @return Regresa un Boolean al realizar la acción
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case android.R.id.home:

                Objects.requireNonNull(drawerLayoutHome).openDrawer(GravityCompat.START);

                break;

            case R.id.itemToolbarUno:


                break;

            case R.id.itemToolbarDos:


                break;

        }

        return super.onOptionsItemSelected(menuItem);

    }

    /**
     * Ejecuta la acción al presionar un item del menu desplegable
     *
     * @param menuItem Recibe un parametro de tipo MenuItem
     *                 para realizar la acción segun el item seleccionado
     */
    public void selectItem(MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case R.id.itemNavUno:

                navigateClima();

                break;

            case R.id.itemNavDos:

                break;

            case R.id.itemNavTres:

                break;

        }

    }

    /*
    #############################################################################################
    Metodo propios de la clase
    #############################################################################################
    */

    /**
     * Metodo redirige a la actividad clima
     */
    public void navigateClima() {

        Intent intent = new Intent(this, ClimaActivity.class);

        startActivity(intent);

    }

    /*
    #############################################################################################
    Implementacion de la interface InterfaceActivity
    Metodos de manejo de contenido
    #############################################################################################
    */

    /**
     * Inicializa la interfaz de usuario
     */
    @Override
    public void setupUI() {

        FragmentHome fragmentHome = new FragmentHome();

        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager
                .beginTransaction()
                .replace(R.id.rlyGeneralContainer,
                        fragmentHome,
                        TagsFragments.tagFragHome)
                .commitAllowingStateLoss();

    }
}
