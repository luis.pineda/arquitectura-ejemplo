package com.example.chris.sections.clima.fragments.weather;

import com.example.chris.base.BasePresenter;
import com.example.chris.repositories.weather.WeatherRepository;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class FragmentWeatherPresenterImpl
        extends BasePresenter<FragmentWeatherView>
        implements FragmentWeatherPresenter {

    /**
     * Variables
     */
    WeatherRepository weatherRepository;

    /**
     * Inyeccion del presentador
     *
     * @param fragmentWeatherView
     * @param weatherRepository
     */
    @Inject
    public FragmentWeatherPresenterImpl(FragmentWeatherView fragmentWeatherView, WeatherRepository weatherRepository) {
        this.view = fragmentWeatherView;
        this.weatherRepository = weatherRepository;
    }


    //region Metodos sobrecargados de las interfaces

    /**
     * Metodo que agrega el oservador al bus de eventos
     */
    @Override
    public void onBind() {
        compositeDisposable.add(weatherRepository.getForecastData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::showWeather));
    }

    //endregion
}