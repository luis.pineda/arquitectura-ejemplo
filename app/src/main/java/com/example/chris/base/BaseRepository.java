package com.example.chris.base;

import com.google.gson.JsonSyntaxException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import retrofit2.HttpException;

/**
 * Base de los repositorios
 */
public class BaseRepository {

    /**
     * Metodo que maneja errores no relacionados con el HttpException
     * Si Throwable no es una excepcion HTTP, crashea la app.
     *
     * @param throwable
     */
    protected void handleNonHttpException(Throwable throwable) {
        //Si no es una HttpException revisa otras opciones
        if (throwable instanceof HttpException) {

        } else if (throwable instanceof JsonSyntaxException) {

        } else if (throwable instanceof SocketTimeoutException) {

        } else if (throwable instanceof ConnectException) {

        } else {
            throw new RuntimeException(throwable);
        }
    }

}
