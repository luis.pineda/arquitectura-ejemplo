package com.example.chris.base;

import retrofit2.Response;

public interface InterfaceErrorHandler {
    void onError(Response response);
}
