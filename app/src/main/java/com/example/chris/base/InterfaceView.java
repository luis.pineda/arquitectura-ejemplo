package com.example.chris.base;

public interface InterfaceView {

    void showLoading(boolean show);

    void showErrorMessage(String errorMessage);

    void showMessage(String message);

}
