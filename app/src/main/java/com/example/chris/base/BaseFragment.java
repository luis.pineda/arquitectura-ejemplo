package com.example.chris.base;

import androidx.fragment.app.Fragment;

import java.util.Objects;

import javax.inject.Inject;

/**
 * Base de los fragmentos de Tipo InterfacePresenter
 *
 * @param <T>
 */
public class BaseFragment<T extends InterfacePresenter>
        extends Fragment
        implements InterfaceView {

    //region Controles y variables

    //Inyeccion del presentador
    @Inject
    public T presenter;

//    ProgressDialog progressDialog;

    //endregion

    /**
     * Metodo para mostrar la barra de progreso
     *
     * @param show
     */
    public void showLoading(boolean show) {

        ((HomeBaseActivity) Objects.requireNonNull(getActivity())).showLoading(show);

//        if (show) {
//            if (progressDialog == null) {
//                progressDialog = ProgressDialog.show(getActivity(), null, "Loading");
//            } else {
//                progressDialog.show();
//            }
//
//        } else {
//            progressDialog.dismiss();
//        }

    }

    //region Metodos sobrecargados de las interfaces

    /**
     * Metodo sobrecargado de InterfaceView
     * Muestra un mensaje de error
     */
    @Override
    public void showErrorMessage(String errorMessage) {

        ((HomeBaseActivity) Objects.requireNonNull(getActivity())).showErrorMessage(errorMessage);

//        AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
//        dialog.setMessage("ERROR");
//        dialog.show();
    }

    /**
     * Metodo sobrecargado de InterfaceView
     * Muestra un mensaje informativo
     */
    @Override
    public void showMessage(String message) {

        ((HomeBaseActivity) Objects.requireNonNull(getActivity())).showMessage(message);

//        AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
//        dialog.setMessage(message);
//        dialog.show();
    }

    /**
     * Metodo sobrecargado de InterfacePresenter
     * Detiene el bus de eventos
     */
    @Override
    public void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

    //endregion

}
