package com.example.chris.base;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;

public class BasePresenter<V extends InterfaceView>
        implements InterfaceErrorHandler {

    /**
     * Variables
     */
    //Maneja el bus de eventos
    public CompositeDisposable compositeDisposable = new CompositeDisposable();

    //Maneja la vista generica
    public V view;

    /**
     * Metodos para detener el bus de eventos
     */
    public void onStop() {
        compositeDisposable.clear();
    }

    /**
     * Implementacion de la interface del manejador de error
     *
     * @param response
     */
    @Override
    public void onError(Response response) {
        view.showErrorMessage(response.message());
    }
}
