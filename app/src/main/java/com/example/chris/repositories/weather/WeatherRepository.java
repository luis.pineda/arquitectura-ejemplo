package com.example.chris.repositories.weather;

import com.example.chris.entities.WeatherData;

import io.reactivex.Observable;

public interface WeatherRepository {
    Observable<WeatherData> getForecastData();
}
