package com.example.chris.dagger.components.subcomponents;

import com.example.chris.dagger.modules.sections.FragmentWindModule;
import com.example.chris.sections.clima.fragments.wind.FragmentWind;

import dagger.Subcomponent;

/**
 * Subcomponente de la app para inyeccion al FragmentWindModule
 */
@Subcomponent(modules = {FragmentWindModule.class})
public interface FragmentWindComponent {

    /**
     * Inyeccion a la instancia del fragmento FragmentWind
     *
     * @param fragmentWind
     */
    void inject(FragmentWind fragmentWind);
}
