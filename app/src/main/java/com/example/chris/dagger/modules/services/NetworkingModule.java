package com.example.chris.dagger.modules.services;

import com.example.chris.BuildConfig;
import com.example.chris.services.networking.ApiService;
import com.example.chris.services.networking.interceptors.QueryParameterInterceptor;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Modulo de servicio de consultas por internet
 * Provee:
 * - Constructor del cliente HTTP con la libreria OkHttpClient
 * - Constructor del consumidor de servicio web con la Libreria Retrofit
 * - Creador del enlace retrofit para consumir un servicio
 */
@Module
public class NetworkingModule {

    //URL Base de donde se consumira el servicio WEB
    private String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    /**
     * Proveedor del constructor del cliente HTTP
     *
     * @return
     */
    @Singleton
    @Provides
    public OkHttpClient provideClient() {

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG)

            clientBuilder.addNetworkInterceptor(new StethoInterceptor());

        clientBuilder.addInterceptor(new QueryParameterInterceptor("appid", "ff287173dfc02d8de3aad212143202e1"));

        clientBuilder.addInterceptor(new QueryParameterInterceptor("units", "imperial"));

        return clientBuilder.build();

    }

    /**
     * Proveedor del constructor del consumidor de servicio web
     *
     * @param client
     * @return
     */
    @Singleton
    @Provides
    public Retrofit provideRetrofit(OkHttpClient client) {

        //Can register type adapters here

        Gson gson = new GsonBuilder().create();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
    }

    /**
     * Proveedor del creador del enlace retrofit para consumir un servicio
     *
     * @param retrofit
     * @return
     */
    @Singleton
    @Provides
    public ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }


}
