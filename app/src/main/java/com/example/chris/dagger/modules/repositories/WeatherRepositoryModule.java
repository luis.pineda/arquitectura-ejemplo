package com.example.chris.dagger.modules.repositories;

import com.example.chris.interactors.database.WeatherDatabaseInteractor;
import com.example.chris.interactors.database.WeatherDatabaseInteractorImpl;
import com.example.chris.interactors.memory.WeatherMemoryInteractor;
import com.example.chris.interactors.memory.WeatherMemoryInteractorImpl;
import com.example.chris.interactors.network.WeatherNetworkInteractor;
import com.example.chris.interactors.network.WeatherNetworkInteractorImpl;
import com.example.chris.repositories.weather.WeatherRepository;
import com.example.chris.repositories.weather.WeatherRepositoryImpl;
import com.example.chris.services.session.SessionService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo de repositorio para el WeatherRepository
 * Provee:
 * - Repositorio con la implementacion del interactuadores y una sessionService
 * - Interactuador para el consumo del WebService
 * - Interactuador para la gestion de la base de datos
 * - Interactuador para el acceso a la memoria??
 */
@Module
public class WeatherRepositoryModule {
    /**
     * Proveedor del interactuador para el consumo del web service
     *
     * @param weatherNetworkInteractor
     * @return
     */
    @Provides
    @Singleton
    public WeatherNetworkInteractor provideWeatherNetwork(WeatherNetworkInteractorImpl weatherNetworkInteractor) {
        return weatherNetworkInteractor;
    }

    /**
     * Proveedor del interactuador para el acceso a memoria??
     *
     * @param weatherMemoryInteractor
     * @return
     */
    @Provides
    @Singleton
    public WeatherMemoryInteractor provideWeatherMemory(WeatherMemoryInteractorImpl weatherMemoryInteractor) {
        return weatherMemoryInteractor;
    }

    /**
     * Proveedor del interactuador para la gestion de la base de datos
     *
     * @param weatherDatabaseInteractor
     * @return
     */
    @Provides
    public WeatherDatabaseInteractor provideWeatherDatabase(WeatherDatabaseInteractorImpl weatherDatabaseInteractor) {
        return weatherDatabaseInteractor;
    }

    /**
     * Proveedor del repositorio con la implementacion del interactuadores y una sessionService
     *
     * @param sessionService
     * @param weatherNetworkInteractor
     * @param weatherDatabaseInteractor
     * @param weatherMemoryInteractor
     * @return
     */
    @Provides
    @Singleton
    public WeatherRepository provideWeatherRepository(SessionService sessionService,
                                                      WeatherNetworkInteractor weatherNetworkInteractor,
                                                      WeatherDatabaseInteractor weatherDatabaseInteractor,
                                                      WeatherMemoryInteractor weatherMemoryInteractor) {

        return new WeatherRepositoryImpl(sessionService,
                weatherNetworkInteractor,
                weatherDatabaseInteractor,
                weatherMemoryInteractor);

    }
}
