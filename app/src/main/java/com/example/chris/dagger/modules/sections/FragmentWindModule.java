package com.example.chris.dagger.modules.sections;

import com.example.chris.sections.clima.fragments.wind.FragmentWindPresenterImpl;
import com.example.chris.sections.clima.fragments.wind.FragmentWindPresenter;
import com.example.chris.sections.clima.fragments.wind.FragmentWindView;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo de fragmento para el FragmentWeather
 * Provee:
 * - Vista del fragmento
 * - Presentador del fragmento
 */
@Module
public class FragmentWindModule {

    //Variables
    FragmentWindView fragmentWindView;

    /**
     * Constructor del modulo
     *
     * @param fragmentWindView
     */
    public FragmentWindModule(FragmentWindView fragmentWindView) {
        this.fragmentWindView = fragmentWindView;
    }

    /**
     * Proveedor de la Vista del fragmento
     *
     * @return
     */
    @Provides
    public FragmentWindView providesView() {
        return fragmentWindView;
    }

    /**
     * Proveedor del presentador del fragmento
     *
     * @param fragmentWindPresenter
     * @return
     */
    @Provides
    public FragmentWindPresenter providesPresenter(FragmentWindPresenterImpl fragmentWindPresenter) {
        return fragmentWindPresenter;
    }
}
