package com.example.chris.dagger.modules.sections;

import com.example.chris.sections.clima.fragments.weather.FragmentWeatherPresenterImpl;
import com.example.chris.sections.clima.fragments.weather.FragmentWeatherPresenter;
import com.example.chris.sections.clima.fragments.weather.FragmentWeatherView;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo de fragmento para el FragmentWeather
 * Provee:
 * - Vista del fragmento
 * - Presentador del fragmento
 */
@Module
public class FragmentWeatherModule {

    //Variables
    FragmentWeatherView fragmentWeatherView;

    /**
     * Constructor del modulo
     *
     * @param fragmentWeatherView
     */
    public FragmentWeatherModule(FragmentWeatherView fragmentWeatherView) {
        this.fragmentWeatherView = fragmentWeatherView;
    }

    /**
     * Proveedor de la Vista del fragmento
     *
     * @return
     */
    @Provides
    public FragmentWeatherView providesView() {
        return fragmentWeatherView;
    }

    /**
     * Proveedor del presentador del fragmento
     *
     * @param fragmentWeatherPresenter
     * @return
     */
    @Provides
    public FragmentWeatherPresenter providesPresenter(FragmentWeatherPresenterImpl fragmentWeatherPresenter) {
        return fragmentWeatherPresenter;
    }
}
