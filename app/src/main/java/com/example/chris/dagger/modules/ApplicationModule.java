package com.example.chris.dagger.modules;

import android.content.Context;

import com.example.chris.ExampleApp;
import com.example.chris.services.session.SessionService;
import com.example.chris.services.session.StorageSessionServiceImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Modulo general de la app.
 * provee:
 * - Contexto de la app
 * - Servicio de sesion
 */
@Module
public class ApplicationModule {

    /**
     * Proveedor del contexto de la app
     *
     * @return
     */
    @Provides
    public Context providesContext() {
        return ExampleApp.getExampleApp();
    }

    /**
     * Proveedor del servicio de sesion
     *
     * @param storageSessionService
     * @return
     */
    @Provides
    public SessionService providesSessionService(StorageSessionServiceImpl storageSessionService) {
        return storageSessionService;
    }

}
