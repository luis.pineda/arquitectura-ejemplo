package com.example.chris.dagger.components.subcomponents;

import com.example.chris.dagger.modules.sections.FragmentWeatherModule;
import com.example.chris.sections.clima.fragments.weather.FragmentWeather;

import dagger.Subcomponent;

/**
 * Subcomponente de la app para inyeccion al FragmentWeather
 */
@Subcomponent(modules = {FragmentWeatherModule.class})
public interface FragmentWeatherComponent {

    /**
     * Inyeccion a la instancia del fragmento FragmentWeather
     *
     * @param fragmentWeather
     */
    void inject(FragmentWeather fragmentWeather);
}
