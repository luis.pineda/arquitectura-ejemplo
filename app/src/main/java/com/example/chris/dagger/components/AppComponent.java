package com.example.chris.dagger.components;

import com.example.chris.ExampleApp;
import com.example.chris.dagger.components.subcomponents.FragmentLocationComponent;
import com.example.chris.dagger.components.subcomponents.FragmentWeatherComponent;
import com.example.chris.dagger.components.subcomponents.FragmentWindComponent;
import com.example.chris.dagger.modules.ApplicationModule;
import com.example.chris.dagger.modules.repositories.WeatherRepositoryModule;
import com.example.chris.dagger.modules.sections.FragmentLocationModule;
import com.example.chris.dagger.modules.sections.FragmentWeatherModule;
import com.example.chris.dagger.modules.sections.FragmentWindModule;
import com.example.chris.dagger.modules.services.DatabaseModule;
import com.example.chris.dagger.modules.services.NetworkingModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Componente general de la app
 * Modulos:
 * - Modulo General de la App
 * - Modulo de servicio para consumo de webservices
 * - Modulo de servicio para gestion de base de datos
 * - Modulo de repositorio para el WeatherRepository
 */
@Component(modules = {
        ApplicationModule.class,
        NetworkingModule.class,
        DatabaseModule.class,
        WeatherRepositoryModule.class})
@Singleton
public interface AppComponent {

    /**
     * Inyeccion a la instancia principal de la App
     *
     * @param exampleApp
     */
    void inject(ExampleApp exampleApp);

    //region Sub Componentes adicionales a usar en la APP

    FragmentWeatherComponent plus(FragmentWeatherModule module);

    FragmentLocationComponent plus(FragmentLocationModule module);

    FragmentWindComponent plus(FragmentWindModule module);

    //endregion

}
