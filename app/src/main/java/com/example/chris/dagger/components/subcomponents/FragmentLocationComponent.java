package com.example.chris.dagger.components.subcomponents;

import com.example.chris.dagger.modules.sections.FragmentLocationModule;
import com.example.chris.sections.clima.fragments.location.FragmentLocation;

import dagger.Subcomponent;

/**
 * Subcomponente de la app para inyeccion al FragmentLocation
 */
@Subcomponent(modules = {FragmentLocationModule.class})
public interface FragmentLocationComponent {

    /**
     * Inyeccion a la instancia del fragmento FragmentLocation
     *
     * @param fragmentLocation
     */
    void inject(FragmentLocation fragmentLocation);
}
