package com.example.chris;

import android.app.Application;

import com.example.chris.dagger.components.AppComponent;
import com.example.chris.dagger.components.DaggerAppComponent;
import com.facebook.stetho.Stetho;

import net.danlew.android.joda.JodaTimeAndroid;

public class ExampleApp extends Application {

    /**
     * Variables
     */
    private static AppComponent appComponent;

    private static ExampleApp exampleApp;

    /**
     * Getters
     */
    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static ExampleApp getExampleApp() {
        return exampleApp;
    }

    /**
     * Sobrecarga del metodo on Create
     */
    @Override
    public void onCreate() {

        super.onCreate();

        injectDependencies();

        Stetho.initializeWithDefaults(this);

        JodaTimeAndroid.init(this);

        exampleApp = this;

    }

    /**
     * Metodo para inyectar dependencias
     */
    private void injectDependencies() {

        appComponent = DaggerAppComponent
                .builder()
                .build();

        appComponent.inject(this);

    }

}
