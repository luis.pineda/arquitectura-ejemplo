package com.example.chris.services.networking.interceptors;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Interceptor que gestiona los parametros de la consulta
 */
public class QueryParameterInterceptor
        implements Interceptor {

    //region Variables

    private String key;

    private String value;

    //endregion

    /**
     * Constructor de la clase
     *
     * @param key
     * @param value
     */
    public QueryParameterInterceptor(String key,
                                     String value) {
        this.key = key;

        this.value = value;

    }

    /**
     * Sobrecarga del metodo de la interface
     *
     * @param chain
     * @return
     * @throws IOException
     */
    @Override
    public Response intercept(Chain chain) throws IOException {

        Request original = chain.request();

        HttpUrl originalHttpUrl = original.url();

        HttpUrl url = originalHttpUrl
                .newBuilder()
                .addQueryParameter(key, value)
                .build();

        // Request customization: add request headers
        Request.Builder requestBuilder = original
                .newBuilder()
                .url(url);

        Request request = requestBuilder.build();

        return chain.proceed(request);
    }

}
