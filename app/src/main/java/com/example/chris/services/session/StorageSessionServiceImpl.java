package com.example.chris.services.session;

import android.content.Context;
import android.preference.PreferenceManager;

import javax.inject.Inject;

public class StorageSessionServiceImpl
        implements SessionService {

    private static final String LOCATION_KEY = "location";

    Context context;

    @Inject
    public StorageSessionServiceImpl(Context context) {
        this.context = context;
    }

    @Override
    public void saveLocation(String name) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(LOCATION_KEY, name)
                .apply();
    }

    //If the user hasn't set a location, we just default to London.
    @Override
    public String getLocation() {
        return PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(LOCATION_KEY, "Villavicencio");
    }
}
