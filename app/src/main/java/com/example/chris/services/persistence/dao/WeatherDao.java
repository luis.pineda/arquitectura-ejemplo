package com.example.chris.services.persistence.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.chris.entities.WeatherData;

import io.reactivex.Maybe;

/**
 * DAO de la base de datos para la entidad WeatherData
 */
@Dao
public interface WeatherDao {
    /**
     * Consulta a la entidad WeatherData
     *
     * @param name
     * @return
     */
    @Query("SELECT * FROM WeatherData WHERE name = :name")
    Maybe<WeatherData> getWeather(String name);

    /**
     * Insercion a la entidad WeatherData
     *
     * @param weatherData
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveData(WeatherData weatherData);
}
