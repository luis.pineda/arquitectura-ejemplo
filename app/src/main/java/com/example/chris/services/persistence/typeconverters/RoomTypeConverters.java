package com.example.chris.services.persistence.typeconverters;

import androidx.room.TypeConverter;

import org.joda.time.DateTime;

/**
 * Conversores de tipo para gestion de datos
 */
public class RoomTypeConverters {

    /**
     * Conversor de tipo para gestion de fechas
     * Convierte de Long a Fecha
     *
     * @param timestamp
     * @return
     */
    @TypeConverter
    public DateTime convertLongToDateTime(long timestamp) {
        return new DateTime(timestamp);
    }

    /**
     * Conversor de tipo para gestion de fechas
     * Convierte de Fecha a Long
     *
     * @param dateTime
     * @return
     */
    @TypeConverter
    public long convertDateTimeToLong(DateTime dateTime) {
        return dateTime.getMillis();
    }
}
