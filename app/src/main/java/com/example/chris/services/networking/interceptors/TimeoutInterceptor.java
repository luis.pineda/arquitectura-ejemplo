package com.example.chris.services.networking.interceptors;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Interceptor para el Timeout de la consulta
 */
public class TimeoutInterceptor
        implements Interceptor {

    /**
     * Sobrecarga del metodo de la interface
     *
     * @param chain
     * @return
     * @throws IOException
     */
    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();

        Response response = chain.proceed(request);

        chain.withConnectTimeout(5, TimeUnit.SECONDS);

        chain.withReadTimeout(5, TimeUnit.SECONDS);

        chain.withWriteTimeout(5, TimeUnit.SECONDS);

        return response;

    }

}
