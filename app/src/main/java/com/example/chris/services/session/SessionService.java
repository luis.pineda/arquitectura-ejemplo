package com.example.chris.services.session;

public interface SessionService {

    void saveLocation(String name);

    String getLocation();

}
